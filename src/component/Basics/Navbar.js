import React from 'react';
import Menu from './menuApi';


export const Navbar = ({ filterItem, menuList }) => {
    return (
        <>
            <nav className='navbar'>
                <div className='btn-group'>
                    {
                        menuList.map((curElm) => {
                            return <button className='btn-group__item' onClick={() => { filterItem(curElm) }}>
                                {curElm}
                            </button>
                        })
                    }
                </div>
            </nav>
        </>
    )
}
