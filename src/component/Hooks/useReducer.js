import React, { useReducer } from "react";
import "./style.css";



const reducer = (state, dispatch) => {
    if (dispatch.type === "INCR") {
        state = state + 1;
    }
    if (state > 0 && dispatch.type === "DCR") {
        state = state - 1;
    }
    return state;
}
const UseReducer = () => {
    const initialData = 0;
    ///useReducer() hook lai tala ko line of code ma define gareko xa hai ta
    const [state, dispatch] = useReducer(reducer, initialData)

    return (
        <>
            <div className="center_div">
                <p>{state}</p>
                <div className="button2" onClick={() => dispatch({ type: "INCR" })}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    INCR
                </div>
                <div
                    className="button2"
                    onClick={() => dispatch({ type: "DCR" })}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    DECR
                </div>
            </div>
        </>
    );
};

export default UseReducer;